package com.shepherd.nfctest3

import android.nfc.NfcAdapter
import android.nfc.tech.MifareClassic
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private val TAG = this.javaClass.simpleName

    private var nfcAdapter: NfcAdapter? = null
    private lateinit var mifareClassic: MifareClassic
    private val flag = NfcAdapter.FLAG_READER_NFC_A

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        findViewById<AppCompatButton>(R.id.btnWrite).setOnClickListener {
            writeRawHexToBlock({
                Log.e(TAG, "onCreate: written: $it")
            }, 1, "0123456789ABCDEF", null)
        }
        findViewById<AppCompatButton>(R.id.btnRead).setOnClickListener {
            readSector({
                Log.e(TAG, "onCreate: $it")
            }, 0, null)
        }
    }

    private fun readBlock(callback: (String) -> Unit, blockIndex: Int, password: String?) {
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                val sectorPassword: ByteArray = if (password.isNullOrEmpty()) {
                    MifareClassic.KEY_DEFAULT
                } else {
                    Utils.rawHexToByteArray(hex = password)
                }

                mifareClassic = MifareClassic.get(tag)
                mifareClassic.connect()
                val sectorIndex = mifareClassic.blockToSector(blockIndex)
                mifareClassic.authenticateSectorWithKeyA(sectorIndex, sectorPassword)
                var blockBytes = mifareClassic.readBlock(blockIndex)
                if (blockBytes.size < 16) {
                    throw IOException()
                }
                if (blockBytes.size > 16) {
                    blockBytes = blockBytes.copyOf(16)
                }
                Log.d(TAG, "readBlock: ${Utils.byteArray2Hex(blockBytes)}")
                callback.invoke(Utils.byteArray2Hex(blockBytes) ?: "")
            } catch (e: Exception) {
                Log.e(TAG, "readBlock: 404, ${e.localizedMessage}")
            } finally {
                mifareClassic.close()
                nfcAdapter?.disableReaderMode(this)
            }
        }, flag, null)
    }

    private fun writeBlock(
        callback: () -> Unit,
        blockIndex: Int,
        message: String,
        password: String?
    ) {
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                val sectorPassword: ByteArray = if (password.isNullOrEmpty()) {
                    MifareClassic.KEY_DEFAULT
                } else {
                    Utils.rawHexToByteArray(hex = password)
                }
                var messageAsHex = Utils.byteArray2Hex(message.toByteArray())
                val diff = 32 - messageAsHex!!.length
                messageAsHex = "$messageAsHex${"0".repeat(diff)}"
                Log.d(TAG, "writeBlockOfSector: $messageAsHex")
                mifareClassic = MifareClassic.get(tag)
                mifareClassic.connect()
                val sectorIndex = mifareClassic.blockToSector(blockIndex)
                mifareClassic.authenticateSectorWithKeyA(sectorIndex, sectorPassword)
                mifareClassic.writeBlock(
                    blockIndex,
                    Utils.hex2ByteArray(messageAsHex)
                )
            } catch (e: Exception) {
                Log.e(TAG, "writeMifare: ", e)
            } finally {
                mifareClassic.close()
                nfcAdapter?.disableReaderMode(this)
                callback.invoke()
            }
        }, flag, null)
    }

    private fun overwriteBlock(
        callback: (Map<String, String>) -> Unit,
        blockIndex: Int,
        message: String,
        password: String?
    ) {
        var toWrite: ByteArray = byteArrayOf()
        var tagId: String? = null
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                val sectorPassword: ByteArray = if (password.isNullOrEmpty()) {
                    MifareClassic.KEY_DEFAULT
                } else {
                    Utils.rawHexToByteArray(hex = password)
                }
                Log.d(TAG, "overwriteBlock: $message")

                mifareClassic = MifareClassic.get(tag)
                mifareClassic.connect()
                tagId = Utils.byteArray2Hex(tag.id)!!
                val sectorIndex = mifareClassic.blockToSector(blockIndex)
                mifareClassic.authenticateSectorWithKeyA(sectorIndex, sectorPassword)
                toWrite = mifareClassic.readBlock(blockIndex)
                val decList: ArrayList<Int> = arrayListOf()
                for (i in message.indices step 2) {
                    val temp = "${message[i]}${message[i + 1]}"
                    decList.add(temp.toInt(radix = 16))
                }
                for (i in decList.indices) {
                    toWrite[i] = (decList[i] + toWrite[i].toInt()).toByte()
                }
                mifareClassic.writeBlock(blockIndex, toWrite)

            } catch (e: Exception) {
                Log.e(TAG, "writeMifare: ", e)
            } finally {
                mifareClassic.close()
                nfcAdapter?.disableReaderMode(this)
            }
            // val resultMap = mapOf("minutes" to Utils.byteArray2Hex(toWrite)!!, "cardId" to tagId)
            val resultMap = mutableMapOf<String, String>()
            resultMap["minutes"] = Utils.byteArray2Hex(toWrite)!!
            resultMap["cardId"] = tagId!!
            callback.invoke(resultMap)
        }, flag, null)
    }

    private fun readAll(callback: (Map<Int, List<String>>) -> Unit, password: String?) {
        Log.d(TAG, "readAll")
        val response = mutableMapOf<Int, List<String>>()
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                val sectorPassword: ByteArray = if (password.isNullOrEmpty()) {
                    MifareClassic.KEY_DEFAULT
                } else {
                    Utils.rawHexToByteArray(hex = password)
                }
                mifareClassic = MifareClassic.get(tag)
                mifareClassic.connect()
                for (i in 0 until mifareClassic.sectorCount) {
                    mifareClassic.authenticateSectorWithKeyA(i, sectorPassword)
                    response[i] = Utils.printEntireBlock(mifareClassic, i)
                }
                callback.invoke(response)

            } catch (e: Exception) {
                Log.e(TAG, "readAll: ", e)
                //activity.runOnUiThread { result.error("404", e.localizedMessage, null) }

            } finally {
                mifareClassic.close()
                nfcAdapter?.disableReaderMode(this)
            }
        }, flag, null)
    }

    private fun readSector(
        callback: (ArrayList<String>) -> Unit,
        sectorIndex: Int,
        password: String?
    ) {
        Log.d(TAG, "readSector: Sector Index -> $sectorIndex")
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                val sectorPassword: ByteArray = if (password.isNullOrEmpty()) {
                    MifareClassic.KEY_DEFAULT
                } else {
                    Utils.rawHexToByteArray(hex = password)
                }
                mifareClassic = MifareClassic.get(tag)
                mifareClassic.connect()
                mifareClassic.authenticateSectorWithKeyA(sectorIndex, sectorPassword)
                val sector = Utils.printEntireBlock(mifareClassic, sectorIndex)
                callback.invoke(sector)
                //activity.runOnUiThread { result.success(sector) }
            } catch (e: Exception) {
                //activity.runOnUiThread { result.error("404", e.localizedMessage, null) }
            } finally {
                mifareClassic.close()
                nfcAdapter?.disableReaderMode(this)
            }
        }, flag, null)
    }

    private fun writeRawHexToBlock(
        callback: (String) -> Unit,
        blockIndex: Int,
        message: String,
        password: String?
    ) {
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                val sectorPassword: ByteArray = if (password.isNullOrEmpty()) {
                    MifareClassic.KEY_DEFAULT
                } else {
                    Utils.rawHexToByteArray(hex = password)
                }
                mifareClassic = MifareClassic.get(tag)
                mifareClassic.connect()
                val sectorIndex = mifareClassic.blockToSector(blockIndex)
                Log.d(TAG, "writeRawBlockOfSector: $sectorIndex $blockIndex")
                mifareClassic.authenticateSectorWithKeyA(sectorIndex, sectorPassword)
                val toWrite = Utils.rawHexToByteArray(hex = message)
                Log.d(TAG, "writeRawBlockOfSector: ${toWrite.size}")
                Log.d(TAG, "writeRawBlockOfSector: $toWrite")
                mifareClassic.writeBlock(blockIndex, toWrite)
            } catch (e: Exception) {
                Log.e(TAG, "writeMifare: ", e)
            } finally {
                mifareClassic.close()
                nfcAdapter?.disableReaderMode(this)
            }
            callback.invoke(message)
        }, flag, null)
    }

    private fun sectorCount(callback: (Int) -> Unit) {
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                mifareClassic = MifareClassic.get(tag)
                callback.invoke(mifareClassic.sectorCount)
            } catch (e: Exception) {
                Log.e(TAG, "writeMifare: ", e)
                //  activity.runOnUiThread { result.error("404", e.localizedMessage, null) }
            } finally {
                nfcAdapter?.disableReaderMode(this)
            }
        }, flag, null)
    }

    private fun blockCount(callback: (Int) -> Unit) {
        nfcAdapter?.enableReaderMode(this, { tag ->
            try {
                mifareClassic = MifareClassic.get(tag)
                callback.invoke(mifareClassic.blockCount)
            } catch (e: Exception) {
                Log.e(TAG, "writeMifare: ", e)
                //activity.runOnUiThread { result.error("404", e.localizedMessage, null) }
            } finally {
                nfcAdapter?.disableReaderMode(this)
            }
        }, flag, null)
    }
}